// Create a function called rejectWithFirstChar that takes a string, and returns a Promise that will be rejected with the first character of the passed string, after 500 milliseconds. You may use the delay function from the previous exercise.

const delay = require('./delay.js');

// const rejectWithFirstChar = (str) => {
//   return new Promise((resolve, reject) => {
//       const firstChar = str.charAt(0);
//       setTimeout(() => reject(firstChar), 500);
//     })
// }

// use delay function
const rejectWithFirstChar = (str) => delay(500)
  .then(() => Promise.reject(str.charAt(0)));

rejectWithFirstChar('summer')
  .catch((result) => console.log('First character ', result));

// Create a function called rejectWithLastChar that takes a string, and returns a Promise that will be rejected with the last character of the passed string, after 500 milliseconds. You may use the delay function from the previous exercise.

// const rejectWithLastChar = (str) => {
//   return new Promise((resolve, reject) => {
//       const lastChar = str.slice(-1);
//       setTimeout(() => reject(lastChar), 500);
//     })
// }

// use delay function
const rejectWithLastChar = (str) => delay(500)
  .then(() => Promise.reject(str.slice(-1)));

rejectWithLastChar('sunny')
  .catch((result) => console.log('Last character ', result));

// Create a function called rejectWithFirstAndLastChar that takes a string, and returns a Promise that will be rejected with the first and last character of the passed string. This function should use rejectWithFirstChar and rejectWithLastChar in sequence.

const rejectWithFirstAndLastChar = (str) => rejectWithFirstChar(str)
          .catch(first => rejectWithLastChar(str)
            .catch(last => Promise.reject(first + last)))

rejectWithFirstAndLastChar('nice weather').catch(console.log);
