// Exercise 1 (Build promise)
// Create a function called delay that takes a number of milliseconds, and returns a Promise that will be resolved with no value after the requested delay has passed.
// Use your delay function to print messages "ONE", "TWO", "THREE", "...LIFTOFF!" to the console with a 1 second delay between each message

const delay = require('./delay.js');

delay(1000)
  .then(() => {
    console.log('ONE');
    return delay(1000)
  })
  .then(() => {
    console.log('TWO');
    return delay(1000)
  })
  .then(() => {
    console.log('THREE');
    return delay(1000)
  })
  .then(() => console.log('...LIFTOFF'));
