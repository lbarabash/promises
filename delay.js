module.exports = (milliseconds) => new Promise((resolve, reject) => {
    setTimeout(() => resolve(), milliseconds);
  })
