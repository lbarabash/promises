// Exercise 2 (Chain promises)
// - Create a function called getFirstChar that takes a string, and returns a Promise that will be resolved with the first character of the passed string, after 500 milliseconds. You may use the delay function from the previous exercise.

const delay = require('./delay.js');

// const getFirstChar = (str) => {
//   return new Promise((resolve, reject) => {
//       const firstChar = str.charAt(0);
//       setTimeout(() => resolve(firstChar), 500);
//     })
// }

// use delay function
const getFirstChar = (str) => delay(500).then(() => str.charAt(0));

getFirstChar('hello')
  .then((result) => console.log('First character ', result));

// - Create a function called getLastChar that takes a string, and returns a Promise that will be resolved with the last character of the passed string, after 500 milliseconds. You may use the delay function from the previous exercise.

// const getLastChar = (str) => {
//   return new Promise((resolve, reject) => {
//       const lastChar = str.slice(-1);
//       setTimeout(() => resolve(lastChar), 500);
//     })
// }

// use delay function
const getLastChar = (str) => delay(500).then(() => str.slice(-1));

getLastChar('hello')
  .then((result) => console.log('Last character ', result));

// - Create a function called getFirstAndLastCharSeq that takes a string, and returns a Promise that will be resolved with the first and last character of the passed string. This function should use getFirstChar and getLastChar in sequence.

const getFirstAndLastCharSeq = (str) => getFirstChar(str)
          .then(first => getLastChar(str)
            .then(last => first + last))

getFirstAndLastCharSeq('nice weather').then(console.log)
