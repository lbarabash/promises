// Create a function called getFirstAndLastCharParallel that takes a string, and returns a Promise that will be resolved with the first and last character of the passed string. This function should use getFirstChar and getLastChar in parallel, using Promise.all.
const getFirstChar = (str) => {
  return new Promise((resolve, reject) => {
      const firstChar = str.charAt(0);
      setTimeout(() => resolve(firstChar), 500);
    })
}

const getLastChar = (str) => {
  return new Promise((resolve, reject) => {
      const firstChar = str.slice(-1);
      setTimeout(() => resolve(firstChar), 500);
    })
}

const getFirstAndLastCharSeq = (str) => Promise.all([getFirstChar(str), getLastChar(str)]).then((result => result.join()));

getFirstAndLastCharSeq('nice').then(console.log);
